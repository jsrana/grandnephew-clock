#!/usr/bin/env bash

##    Grandnephew Clock
##    Script that marks the passage of time like a grandfather clock.
#
##    Copyright (C) 2020  June Sage Rana
#
##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
#
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
#
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see 
##    <http://www.gnu.org/licenses/>.


# Chimes played in seqence form a C-major arpeggio. 
# The hour at hand (12-hour format)+3 in binary determines tolling pattern.  
# Play first note (c3) of arpeggio if 4th bit is 1
# Play second (e3) .. if 3rd bit is 1
# Play third (g3) .. if 2nd bit is 1
# Play fourth note (c4, the octave) if 1st bit is 1
#
# hour - 0b+3 - sequence  
# 0001 | 0100 | e3
# 0002 | 0101 | e3,c4
# 0003 | 0110 | e3,g3
# 0004 | 0111 | e3,g3,c4
# 0005 | 1000 | c3
# 0006 | 1001 | c3,c4
# 0007 | 1010 | c3,g3
# 0008 | 1011 | c3,g3,c4
# 0009 | 1100 | c3,e3
# 0010 | 1101 | c3,e3,c4
# 0011 | 1110 | c3,e3,g3
# 0012 | 1111 | c3,e3,g3,c4

toll()
{

    declare -i hour=(`date +%I`+3)

    # Play first note of arpeggio if 4th-bit is 1
    if test $hour -gt 8; then
        declare -i hour=($hour-8)
        mpv ac3/C3.ac3 &
        sleep 2
    fi

    if test $hour -gt 4; then
        declare -i hour=($hour-4)
        mpv ac3/E3.ac3 &
        sleep 2
    fi
    
    if test $hour -gt 2; then
        declare -i hour=($hour-2)
        mpv ac3/G3.ac3 &
        sleep 2
    fi
    
    if test $hour -gt 1; then
        declare -i hour=($hour-1)
        mpv ac3/C4.ac3 &
    fi

}

#while [ 1 ]; do
    
    declare -i start_minute=`date +%M` start_seconds=`date +%S`
    
    ( sleep $(( (60 - $start_seconds + (60 - $start_minute) * 60) - 1 )); toll() ) & ;

#done
